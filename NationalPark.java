//Made by Safin Haque 2132492
import java.util.Random;
import java.util.Scanner;

public class NationalPark{
	public static void main(String[]args){
		Scanner reader=new Scanner(System.in);
		
		
		Cow[] herdOfCows=new Cow[2];
		
		int age;
		double size;
		boolean isHeifer;
		
		for(int i=0;i<herdOfCows.length;i++)
		{
			herdOfCows[i]=new Cow(14, 220.0, true);
		}
		
		for(int j=0;j<herdOfCows.length;j++)
		{
			System.out.println("How old is cow " + (j+1) + " in Months?");
			 age= reader.nextInt();
			
				while(age<=0)
				{
					System.out.println("invalid age, try again");
					age= reader.nextInt();
				}
			
			System.out.println("How much does cow " + (j+1) + " weigh in kg?");
			 size=reader.nextDouble();
				while(size<=0)
				{
					System.out.println("invalid weight number, try again");
					size=reader.nextDouble();
				}
			
			System.out.println("Is cow " + (j+1) + " a Heifer? true or false");
			 isHeifer=reader.nextBoolean();
			
			
			herdOfCows[j]=new Cow(age, size, isHeifer);
			
		}
		System.out.println("Last cow is " + herdOfCows[1].getAgeInMonths() + " years old");
		
		System.out.println("Last cow weighs " + herdOfCows[1].getSizeInKg() + " kg");
		
		System.out.println("Is the cow a heifer?: " + herdOfCows[1].getIsHeifer());
		

		System.out.println("Looks like the computer busted up on the last animals info, Whats their age");
		age=reader.nextInt();
		
		System.out.println("The last cows weight");
		size=reader.nextDouble();
		
		System.out.println("Finally are they a Heifer");
		isHeifer=reader.nextBoolean();
		
		herdOfCows[1].setAgeInMonths(age);
		herdOfCows[1].setSizeInKg(size);
		herdOfCows[1].setIsHeifer(isHeifer);
		
		System.out.println("Last cow is " + herdOfCows[1].getAgeInMonths() + " years old");
		
		System.out.println("Last cow weighs " + herdOfCows[1].getSizeInKg() + " kg");
		
		System.out.println("Is the cow a heifer?: " + herdOfCows[1].getIsHeifer());
		
		
		System.out.println("How much weight will your second cow lose? 10-20%");
		int percent= reader.nextInt();
		
		while(percent>20 || percent<10) //When the input is out of the range
		{
			System.out.println("This percent isn't possible for cows, please re-enter");
			percent=reader.nextInt();
		}
		System.out.println();
		
		System.out.println(herdOfCows[0].getAgeInMonths());
		System.out.println(herdOfCows[0].getSizeInKg());
		System.out.println(herdOfCows[0].getIsHeifer());
		
		System.out.println();
		
		double cow2Weight=herdOfCows[1].weightLoss(percent);
		
		System.out.println("Cow 2 is " + herdOfCows[1].getAgeInMonths() + " years old and will weigh " + cow2Weight + "kg after giving birth. Brought to you by Safin Haque");
		
		
	}
	
}