public class Cow{
	private boolean isHeifer;
	private int ageInMonths;
	private double sizeInKg;
	
	
	public Cow(int age, double size, boolean isHeifer){
		this.ageInMonths=age;
		
		this.isHeifer=isHeifer;
		
		this.sizeInKg=size;
	}
	public int getAgeInMonths(){
		return this.ageInMonths;
	}
	
	public void setAgeInMonths(int age)
	{
		if(age>0)
		{
			this.ageInMonths= age;
		}
	}
	
	public double getSizeInKg(){
		return this.sizeInKg;
	}
	
	public void setSizeInKg(double size){
		if(size>0)
		{
			this.sizeInKg=size;
		}
	}
	
	public boolean getIsHeifer(){
		return this.isHeifer;
	}
	
	public void setIsHeifer(boolean isHeifer)
	{
		this.isHeifer=isHeifer;
	}
	
	
	
	public void cowBio(){
		
		System.out.println("The cow is " + this.ageInMonths + " months old.");
		System.out.println("The cow also weighs " + this.sizeInKg + "kg.");
		
	}
	
	public void BreedorNay(){
		
		if(this.isHeifer==true && ageInMonths>=12 && ageInMonths<=14)
		{
			System.out.println("The Heifer is ready to breed for the coming season");
		}
		else if(isHeifer==false && ageInMonths<12)
		{
			System.out.println("This Heifer needs to mature still");
		}
		else if(isHeifer==false && ageInMonths>14)
		{
			System.out.println("This cow has already been bred with");
		}
		
	}
	
	//Note to self: Cow loses either 10-15% of their bodyweight after giving Birth.
	public double weightLoss(int percentLoss){
		double weight=0;
		
		if(isHeifer=true && sizeInKg>0)
		{
			weight=sizeInKg-(sizeInKg*(percentLoss/100.0));
		}
		else
		{
			weight=sizeInKg;
		}	
		return weight;
	}
}
